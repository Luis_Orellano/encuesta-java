/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuestas;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luis
 */
public class Menu {

    private static Menu instance;

    private BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private Scanner sc = new Scanner(System.in);

    private List<Pregunta> preguntas;
    private List<Persona> personas;

    private Menu() {
        this.preguntas = new ArrayList<>();
        this.personas = new ArrayList<>();
    }

    public static Menu getInstance() {
        if (instance == null) {
            instance = new Menu();
        }
        return instance;
    }

    public void initMenu() {

        int opc = 0;
        do {
            try {
                System.out.println("1- Ingresar Pregunta");
                System.out.println("2- Listar Preguntas");
                System.out.println("3- Responder Preguntas");
                System.out.println("4- Ver datos de personas");
                System.out.println("5- Ver resultado de encuesta");

                System.out.println("0- Salir");

                opc = this.sc.nextInt();

                opciones(opc);
            } catch (InputMismatchException ex) {
                System.out.println("Debe ingresar una opcion valida para continuar");
                this.sc.next();
            }
        } while (opc > 0 || opc < 4);

    }

    private void opciones(int opc) {
        switch (opc) {
            case 1:
                System.out.println("Ingrese una pregunta");
                String pregunta;
                try {
                    pregunta = this.br.readLine();
                    this.preguntas.add(new Pregunta(pregunta));
                } catch (IOException ex) {
                    Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case 2:
                System.out.println("");
                System.out.println("Preguntas:");
                this.preguntas.forEach(System.out::println);
                break;
            case 3:
                Persona nPersona = setPersona();
                System.out.println("Responda con un SI o un NO para cada pregunta");
                String respuesta = "";
                
                for (Pregunta preg : this.preguntas) {
                    System.out.println(preg.getPregunta());
                    respuesta = this.sc.next();
                    nPersona.setRespuesta(respuesta);
                }
                this.preguntas.forEach(System.out::println);
                this.personas.forEach(System.out::println);
                break;
            case 4:
                this.personas.forEach(System.out::println);
                break;
            case 5:
                resultadoEncuesta();
                break;
            case 0:
                salir();
                break;
            default:
                System.out.println("opcion incorrecta");
                break;
        }
    }

    public Persona setPersona() {
        System.out.println("Antes de responder por favor ponga su fecha de nacimiento");
        System.out.println("Ingrese la fecha de nacimiento");
        System.out.println("dia: ");
        String dia = this.sc.next();
        System.out.println("mes: ");
        String mes = this.sc.next();
        System.out.println("año: ");
        String año = this.sc.next();
        String fecha = año + "-" + mes + "-" + dia;
        Persona persona = new Persona(LocalDate.parse(fecha));

        if (persona.obtenerEdad() >= 14 && persona.obtenerEdad() <= 18) {
            persona.setFrangaEtaria("ADOLESCENTE");
        } else if (persona.obtenerEdad() >= 19 && persona.obtenerEdad() <= 29) {
            persona.setFrangaEtaria("JOVENES");
        } else if (persona.obtenerEdad() >= 30 && persona.obtenerEdad() <= 64) {
            persona.setFrangaEtaria("ADULTOS");
        } else if (persona.obtenerEdad() >= 65) {
            persona.setFrangaEtaria("ADULTOS MAYORES");
        } else {
            persona.setFrangaEtaria("FUERA DE RANGO DE EDADES");
        }

        this.personas.add(persona);
        return persona;
    }

    public void resultadoEncuesta() {
        System.out.println("\tResultado de la encuesta");
        int siAdol = 0;
        int noAdol = 0;
        int siJov = 0;
        int noJov = 0;
        int siAdult = 0;
        int noAdult = 0;
        int siAdultM = 0;
        int noAdultM = 0;
        int sizeAux = 0;
        for (Persona per : this.personas) {
            sizeAux = per.getRespuesta().size();
            for (int i = 0; i < sizeAux; i++) {
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADOLESCENTES")) && (per.getRespuesta().get(i).equalsIgnoreCase("SI"))) {
                    siAdol++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADOLESCENTES")) && (per.getRespuesta().get(i).equalsIgnoreCase("NO"))) {
                    noAdol++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("JOVENES")) && (per.getRespuesta().get(i).equalsIgnoreCase("SI"))) {
                    siJov++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("JOVENES")) && (per.getRespuesta().get(i).equalsIgnoreCase("NO"))) {
                    noJov++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADULTOS")) && (per.getRespuesta().get(i).equalsIgnoreCase("SI"))) {
                    siAdult++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADULTOS")) && (per.getRespuesta().get(i).equalsIgnoreCase("NO"))) {
                    noAdult++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADULTOS MAYORES")) && (per.getRespuesta().get(i).equalsIgnoreCase("SI"))) {
                    siAdultM++;
                }
                if ((per.getFrangaEtaria().equalsIgnoreCase("ADULTOS MAYORES")) && (per.getRespuesta().get(i).equalsIgnoreCase("NO"))) {
                    noAdultM++;
                }
            }
            sizeAux = 0;
        }

        System.out.println("ADOLESCENTES: SI=" + siAdol + "/ NO=" + noAdol);
        System.out.println("JOVENES: SI=" + siJov + "/ NO=" + noJov);
        System.out.println("ADULTOS: SI=" + siAdult + "/ NO=" + noAdult);
        System.out.println("ADULTOS MAYORES: SI=" + siAdultM + "/ NO=" + noAdultM);

    }

    private void salir() {
        System.out.println("Saliendo del programa...");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            System.out.println(ex.getMessage());
        }

        System.exit(0);
    }
}
