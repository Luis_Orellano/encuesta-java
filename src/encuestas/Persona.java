/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package encuestas;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Luis
 */
public class Persona {
    private LocalDate fechaNacimiento;
    private String frangaEtaria;
    private List<String> respuesta;

    public Persona(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
        this.respuesta = new ArrayList<>();
    }
    
    public LocalDate parseFecha(){
//        LocalDate localDate = LocalDate.parse(fecha);
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return this.fechaNacimiento = LocalDate.parse(this.getFechaNacimiento().toString(), fmt);
    }
    
    public int obtenerEdad(){
        Period periodo = Period.between(this.getFechaNacimiento(), LocalDate.now());
        return periodo.getYears();
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getFrangaEtaria() {
        return frangaEtaria;
    }

    public void setFrangaEtaria(String frangaEtaria) {
        this.frangaEtaria = frangaEtaria;
    }

    public List<String> getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta.add(respuesta);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.fechaNacimiento);
        hash = 17 * hash + Objects.hashCode(this.frangaEtaria);
        hash = 17 * hash + Objects.hashCode(this.respuesta);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (!Objects.equals(this.frangaEtaria, other.frangaEtaria)) {
            return false;
        }
        if (!Objects.equals(this.respuesta, other.respuesta)) {
            return false;
        }
        if (!Objects.equals(this.fechaNacimiento, other.fechaNacimiento)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Persona{" + "fechaNacimiento=" + fechaNacimiento + ", frangaEtaria=" + frangaEtaria + ", respuesta=" + respuesta + '}';
    }
}
